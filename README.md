# Sun Sensitivity Classification Algorithm
The goal of this algorithm is to help individuals accurately identify 
their sun sensitivity by scoring their responses to a survey on the 
reaction of their skin to sun exposure.

[[_TOC_]]

## Credits
This research was conducted by members of the Marriott Lab at the 
Oregon Health and Science University: Teala Alvord, Mark Sanchez,
Lisa Marriott, Ravikant Samathan, and Medina Lamkin.

## License
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

## Survey Questions

**Q1: If, after several months of not being in the sun, you stayed outdoors 
for about 1 hour at noon for the first time in the summer without sunscreen, 
what would happen to your skin in the first 24 hours? <br>**
&emsp;&emsp;a - Always sunburn <br>
&emsp;&emsp;b - Sunburn usually <br>
&emsp;&emsp;c - Sunburn minimally <br>
&emsp;&emsp;d - Rarely sunburn <br>
&emsp;&emsp;e - Never sunburn <br>

**Q2: What would be your skin’s reaction within the first 24 hours? 
(Would the sun-exposed area become pink/red, irritated, tender, or itchy?) <br>**
&emsp;&emsp;a - Painful sunburn, with blisters<br>
&emsp;&emsp;b - Painful sunburn, with peeling<br>
&emsp;&emsp;c - Severe (painful) burning, skin irritation, tenderness or itching​<br>
&emsp;&emsp;d - Mild burning, skin irritation, tenderness or itching​<br>
&emsp;&emsp;e - Minimal skin irritation, tenderness or itching​<br>
&emsp;&emsp;f - Occasional (Rare) skin irritation, tenderness, or itching​<br>
&emsp;&emsp;g - No skin irritation, tenderness, or itching​<br>

**Q3: Over the next 7 days, would you develop a tan?<br>**
&emsp;&emsp;a - Never tan (includes having red, burned skin)​<br>
&emsp;&emsp;b - Tan minimally<br>
&emsp;&emsp;c - Tan moderately<br>
&emsp;&emsp;d - Tan deeply<br>
&emsp;&emsp;e - No noticeable change in skin color​<br>


## Required packages (dependencies):
    numpy, pandas, sklearn, matplotlib, openpyxl
