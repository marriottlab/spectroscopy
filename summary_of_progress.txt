This document details my understanding of the work done thus far for
the spectroscopy project and the background that the project is based on.

- Fitzpatrick writes paper describing 4 different skin types.

- Suggestion to add 2 more skin types to include persons of darker skin tone.

- Eiler (2013) decides to evaluate the accurateness of the classifications
	> Uses a spectrophotometer to measure melanin
	> Study participants administered survey by a dermatologist
	> Dermatologist classifies them using the FST survey
	> Compare the results of classification to the melanin density
	> Sunburning and tanning are suspected to be problematic
	  terminology

- Liasi et al. developed a spectroscopy tool to measure the melanin density of skin. We have readings from this. This data is used as an external validation.

- Teala and Mark work on developing an algorithm that would assign
  values to each possible answer and categorize them into the correct
  FST group.
	> These are the values Mark assigned to each answer option 
		q1 = 1, 25, 50, 75, 100
        	q2 = 0, 5, 9, 12, 48, 49, 50
        	q3 = 0, 11, 25, 71, 150

	> Correct skin type determined by participant

    > Scoring accuracy 91.5% to 92.5% depending on size of dataset

- Conditional scoring: Participants answer question 3 differently on the opposite sides of the spectrum due to different understandings of answer wording.

- My concerns:
	> We do not have enough data to properly test the algorithm
	> What are the cutoff categorization points for Mark's scoring?
	> Does SPSS implement the scoring or is it just statistical 
	  analysis?
	> Eiler seems to have much more data; possiblity to use that?
