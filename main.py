from data import Data
from scoring import Scoring
from analysis import Analysis


def main():
    # Create instances of all necessary objects
    #data = Data(spectroscopy_only=True)
    data = Data()
    scoring = Scoring()
    

    # Load data and store how many samples
    q_data, skintype_data = data.load_all_excel_data()
    num_samples = data.how_many_samples()
    num_types = data.num_types
    cutoffs = scoring.get_cutoffs()

    # Mark Sanchez's scoring
    print("\n\nSanchez Scoring:")
    og_file_name = "og_scored"
    og_results, og_scores, og_individual_scores = scoring.original_scoring(num_samples, q_data)

    og_analysis = Analysis(num_samples, skintype_data, q_data, num_types, 
                                og_results, og_scores, cutoffs)
    
    og_analysis.all_analysis()
    
    data.create_expanded_data_file(og_file_name, og_scores, og_results, og_individual_scores)
    data.create_limited_data_file(og_file_name, q_data, skintype_data, og_scores, og_results, og_individual_scores)

    """
    # Conditional scoring using the lower boundary
    print("\n\nConditional Scoring with Lower Boundary:")
    lb_cond_results, lb_cond_scores, lb_individual_scores = scoring.cond_scoring(num_samples, q_data)

    lb_cond_analysis = Analysis(num_samples, skintype_data, q_data, num_types, 
                                    lb_cond_results, lb_cond_scores, cutoffs)
    
    lb_cond_analysis.all_analysis()

    # Analyze distribution of q3 responses according to scores for q1 + q2
    q1_and_q2_scores = scoring.score_1_and_2(num_samples, q_data)
    """
    return

main()
