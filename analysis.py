import numpy as np
import matplotlib.pyplot as plt
import pandas

class Analysis():

    def __init__(self, num_samples, skintype_data, q_data, 
                       num_types, results, scores, cutoffs):
        self.num_samples = num_samples
        self.skintype_data = np.asarray(skintype_data)
        self.q_data = np.asarray(q_data)
        self.num_types = num_types
        self.results = np.asarray(results)
        self.scores = np.asarray(scores)
        self.cutoffs = np.asarray(cutoffs)

    # Calls all the analysis functions
    def all_analysis(self):
#        self.calculate_accuracy()
#        self.incorrect_by_type()
#        self.incorrectly_scored_only()
        self.crosstabs()
#        self.all_data_by_type()
#        self.ans_freq()


    # Calculate the accuracy of a scoring algorithm
    def calculate_accuracy(self):
        correct = 0

        for i in range(self.num_samples):
            if int(self.results[i]) == int(self.skintype_data[i]):
                correct += 1

        accuracy = correct / self.num_samples * 100
        print("Overall accuracy of results:", accuracy, "%")
        

    # Calculate the percentage of each type that was incorrectly classified
    # List the number of each incorrectly classified items that is over/under
    def incorrect_by_type(self):

        # Column 0 is number of incorrectly scored samples
        # Column 1 is for overestimation of sensitivity, under estimation of 
        # type. Scored type < actual type (scored 4, really 5)
        # Column 2 is for underestimation of sensitivitiy, over estimation of 
        # type. Scored type > actual type (scored 4, really 3)
        # Column 3 is total number of samples of the type
        incorrect_by_type = np.zeros((6,4), dtype=float)

        # Count the number of each skin type in our sample and the 
        # number of inaccurate classifications for each skin type
        for i in range(self.num_samples):
            curr_type = 1

            while curr_type < self.skintype_data[i]:
                curr_type += 1
            
            # if the scored result is incorrect
            if self.results[i] != self.skintype_data[i]:
                incorrect_by_type[curr_type-1, 0] += 1
                
                # check if it's over or under estimated
                if self.results[i] < self.skintype_data[i]:
                    incorrect_by_type[curr_type-1, 1] += 1
                else:
                    incorrect_by_type[curr_type-1, 2] += 1

            # increment the total count of that skin type
            incorrect_by_type[curr_type-1, 3] += 1
       
        # Calculate and print the percentage of inaccuracies
        for i in range(self.num_types):
            percent = incorrect_by_type[i, 0]/incorrect_by_type[i, 3] * 100
            print("Type", i+1, ": ", round(percent, 2), "% incorrect")
            print("\t Overestimated sensitivity, underestimated type: ", 
                                incorrect_by_type[i][1]) 
            print("\t Underestimated sensitivity, overestimated type: ", 
                                incorrect_by_type[i][2]) 


    # Print the information about each incorrectly scored data, including scored
    # type, actual type, score, cutoffs for actual type, and question data
    def incorrectly_scored_only(self):
        print("\nHere is the information about each incorrectly scored item")

        for i in range(self.num_samples):
            if self.results[i] != self.skintype_data[i]:
                print("Scored type:", self.results[i], 
                      " Actual:", self.skintype_data[i], 
                      " Score:", self.scores[i], 
                      " Cutoffs:", self.cutoffs[self.skintype_data[i]-1], 
                      " Question Data:", self.q_data[i])
                
    
    # Prints all the data for each skin type, the answers to the 
    # questions, and what the algorithm scored
    def all_data_by_type(self):
        for i in range(self.num_types):
            print("\nAll data for type ", i+1)
            for j in range(self.num_samples):
                if int(self.skintype_data[j]) == i+1:
                    print("Question answers: ", self.q_data[j], 
                            "\tScored type: ", self.results[j])


    # Prints the frequency of each of the answers to q3 for each skin type
    def ans_freq(self):
        ans = [["Always sunburn", "Sunburn usually", "Sunburn minimally",
                "Rarely sunburn", "Never sunburn"],
               ["Painful burn w/ blisters", "Painful sun w/ peeling",
                "Severe burn w/ irritation", "Mild burn w/ irritation",
                "Min skin irritation", "Occasional irritation", 
                "No irritation"],
               ["Never tan", "Tan minimally", "Tan moderately", "Tan deeply", 
                "No noticable change"]]
        num_ans = [5, 7, 5]

        # Loop through skin types to find answers to q3 according to
        # their self-identified type.
        for i in range(self.num_types):
            print("\nSKIN TYPE ", i+1)
            ans_freq = np.zeros([3,7])
            total = 0

            for j in range(self.num_samples):
                if self.skintype_data[j]-1 == i:
                    ans_freq[0][self.q_data[j][0]-1] += 1
                    ans_freq[1][self.q_data[j][1]-1] += 1
                    ans_freq[2][self.q_data[j][2]-1] += 1
                    total += 1

            # Print the frequency of each answer
            for j in range(3):
                print("\tQuestion ", j+1, ":")
                for k in range(num_ans[j]):
                    print("\t\t", ans[j][k], ":\t", ans_freq[j][k],
                            "\t", round(ans_freq[j][k]/total*100, 2))
    
    # Cross tabultion of scored type vs actual type
    def crosstabs(self):
        print(pandas.crosstab(self.results, self.skintype_data))


    # Plot how users respond to q3 based on their scores for q1 + q2
    def dist_q3(self, q1_and_q2_scores):
        q3_answers = self.q_data[:, 2]
        # color = [0xfdb0c0, 0xc0fdaf, 0xaffded, 0xecaffd, 0xc6affd, 0xfdd9af]
        color = ['b', 'g', 'r', 'c', 'm', 'y']
        fig = plt.figure()
        ax = fig.add_subplot()
        for i in range(6):
            x, y = [], []
            num_points = 0

            for j in range(self.num_samples):
                if self.skintype_data[j] == i+1:
                    x.append(q3_answers[j])
                    y.append(q1_and_q2_scores[j])
                    num_points += 1

            legend_label = "Type" + str(i+1)
            ax.scatter(x, y, s=4, c=np.full(num_points, color[i]), label=legend_label)

        plt.xlabel('Answer to Q3')
        plt.ylabel('Scores for Q1 + Q2')
        plt.legend()
        plt.savefig("q3ans_distribution.png")


