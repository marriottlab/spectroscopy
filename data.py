import numpy
import pandas
from datetime import date

# Loads the participants' Eilers question answers and the
# Fitzpatrick skin type from data file "complete_data.csv"
# into 2 different arrays for question data and skin type 
# data.

class Data:
    def __init__(self, spectroscopy_only=False):

        # If True, only loads data containing spectroscopy data
        self.spectroscopy_only = spectroscopy_only
        
        # Number of samples; loads any number of samples
        # First row of our csv is the variable names
        # Change to 0 if there is no header row in the csv
        self.num_samples = 0 

        # Number of different skin types
        self.num_types = 6

        # Holds all the data imported from Excel sheet.
        self.all_data = None

        # Column to join datasets on
        self.participant_id = '' 

        # Hold the column name for where answers to Q1, Q2,
        # and Q3 are in the Excel sheet. Populated in the data 
        # loading function. Needed for creating new data files.
        self.q1_ans = ''
        self.q2_ans = ''
        self.q3_ans = ''
        
        # Necessary if we are using data with spectroscopy only
        self.mean_ventral = ''

    # Return the number of data samples
    def how_many_samples(self):
        return self.num_samples

    
    # Use Pandas to load data from Excel file
    def load_all_excel_data(self):

        # Read in all the data from Excel sheet
        file_name = 'sunsens_data/sunsensitivity_alldata.xlsx'
        self.all_data = pandas.read_excel(file_name)

        # Save the name of the participant id col
        self.participant_id = self.all_data.columns[0]

        # Column for all exclusions. 1 -> keep, exlude otherwise.
        fu_exclude_dich = self.all_data.columns[65]

        # These columns are needed for loading or scoring
        self.q1_ans = self.all_data.columns[2] # Q1 Survey Ans
        self.q2_ans = self.all_data.columns[3] # Q2 Survey Ans
        self.q3_ans = self.all_data.columns[4] # Q3 Survey Ans

        # Data file has a scored type based on algorithm and a user 
        # identified type for where the scored type was incorrect
        scored_type = self.all_data.columns[7] # Scored Skin Type
        real_type = self.all_data.columns[8] # User Identified Type

        # Clean the data to make sure we have the data necessary
        self._clean_excel_data(scored_type, real_type, fu_exclude_dich) 

        # Remove data without associated spectroscopy data if necessary
        if self.spectroscopy_only:
            self.mean_ventral = self.all_data.columns[57]
            self._get_spectroscopy_only(self.mean_ventral)
        
        # Get the question and skin type data; these are numpy arrays
        question_data = self._get_question_data()
        skintype_data = self._get_correct_skintypes(scored_type, real_type)

        # Store the number of data samples
        if question_data.shape[0] == skintype_data.shape[0]:
            self.num_samples = int(question_data.shape[0])
        else:
            print("ERROR: question_data.shape[0] != skintype_data.shape[0]")
            return

        return question_data, skintype_data
        
        
    # Clean the data imported from excel sheet
    # all_data is a pandas excel dataframe object
    # q1_ans, q2_ans, and q3_ans are column names for where
    # the participants responses to q1, q2, and q3 are stored
    def _clean_excel_data(self, scored_type, real_type, fu_exclude_dich):
        
        # Drop any entries where any one of the 3 survey questions 
        # have not been answered, where there isn't a correct type
        # indicated, and where we have indicated that it isn't a 
        # valid data point with fu_exclude_dich
        for index, row in self.all_data.iterrows():

            if (pandas.isna(row[fu_exclude_dich])
                or row[fu_exclude_dich] != 1):
                self.all_data.drop(index, inplace=True)

            elif (pandas.isna(row[self.q1_ans])
                or pandas.isna(row[self.q2_ans])
                or pandas.isna(row[self.q3_ans])):
                self.all_data.drop(index, inplace=True)

            elif (pandas.isna(row[scored_type]) 
                and pandas.isna(row[real_type])):
                self.all_data.drop(index, inplace=True)


    # Called if we only want the data with associated spectroscopy
    # data. Drops data without a mean_ventral value from self.all_data
    def _get_spectroscopy_only(self, mean_ventral):
        for index, row in self.all_data.iterrows():
            if pandas.isna(row[mean_ventral]):
                self.all_data.drop(index, inplace=True)


    # Get the question data from excel data; return array
    def _get_question_data(self):

        question_data = [self.all_data[self.q1_ans].tolist(),
                         self.all_data[self.q2_ans].tolist(),
                         self.all_data[self.q3_ans].tolist()]
        
        # Convert from list to array
        question_data = numpy.asarray(question_data, dtype=int)
        
        # Transpose array so that each row represents the set
        # of answers from a given participant rather than each 
        # row representing all the answers to a given question
        question_data = numpy.transpose(question_data)

        return question_data


    # Merges the two columns, scored_type and real_type into a single 
    # numpy array containing the correct/real skin type
    def _get_correct_skintypes(self, scored_type, real_type):
        # List for storing correct skin type
        skintype_data = []

        for index, row in self.all_data.iterrows():
            if pandas.isna(row[real_type]):
                skintype_data.append(float(row[scored_type]))
            else:
                skintype_data.append(float(row[real_type]))

        return numpy.asarray(skintype_data, dtype=int)


    def create_expanded_data_file(self, file_name, scores, results, individual_scores):
        
        file_date = str(date.today())
        file_ext = '.xlsx'
        file_name = file_name + '_sunsens_' +file_date + file_ext

        new_data = self._create_newdata_dataframe(scores, results, 
                                                            individual_scores) 

        new_sheet = self.all_data.merge(new_data, on=self.participant_id)

        writer = pandas.ExcelWriter(file_name)
        new_sheet.to_excel(writer, index=False)
        writer.save()


    # Create a limited data file with only the question data, skin type data,
    # scores for each question, predicted type, mean ventral data, 
    def create_limited_data_file(self, file_name, q_data, skintype_data, scores, results,
                                    individual_scores):
        
        file_date = str(date.today())
        file_ext = '.xlsx'
        file_name = file_name + '_sunsens_' + file_date + file_ext

        old_data = self._create_olddata_dataframe(q_data, skintype_data)
        new_data = self._create_newdata_dataframe(scores, results, 
                                                            individual_scores) 

        new_sheet = old_data.merge(new_data, on=self.participant_id)

        writer = pandas.ExcelWriter(file_name)
        new_sheet.to_excel(writer, index=False)
        writer.save()
        return

    def _create_olddata_dataframe(self, q_data, skintype_data):

        # Name the new columns
        id_col = self.participant_id
        q1_ans_col = self.q1_ans
        q2_ans_col = self.q2_ans
        q3_ans_col = self.q3_ans
        skintype_col = 'Correct_Skin_Type'

        # Participant IDs for joining dataframes
        participant_ids = self.all_data[self.participant_id].tolist()
        q1_data = q_data[:, 0]
        q2_data = q_data[:, 1]
        q3_data = q_data[:, 2]
        
        # Create a dataframe to stage new data to be added 
        data = pandas.DataFrame({ id_col       : participant_ids,
                                      q1_ans_col   : q1_data,
                                      q2_ans_col   : q2_data,
                                      q3_ans_col   : q3_data,
                                      skintype_col : skintype_data
                                     })

        return data


    def _create_newdata_dataframe(self, scores, results, individual_scores):

        # Name the new columns
        id_col = self.participant_id
        total_scores_col = 'Cond_Alg_Total_Scores'
        results_col = 'Cond_Alg_Results'
        q1_scores_col = 'Cond_Alg_Q1_Scores'
        q2_scores_col = 'Cond_Alg_Q2_Scores'
        q3_scores_col = 'Cond_Alg_Q3_Scores'

        # Split individual_scores into separate arrays for each question
        q1_scores = individual_scores[: , 0]
        q2_scores = individual_scores[: , 1]
        q3_scores = individual_scores[: , 2]


        # Participant IDs for joining dataframes
        participant_ids = self.all_data[self.participant_id].tolist()
        
        # Create a dataframe to stage new data to be added 
        new_data = pandas.DataFrame({ id_col           : participant_ids,
                                      total_scores_col : scores,
                                      results_col      : results,
                                      q1_scores_col    : q1_scores,
                                      q2_scores_col    : q2_scores,
                                      q3_scores_col    : q3_scores
                                     })

        return new_data

