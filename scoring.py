import numpy as np
from sklearn.cluster import KMeans

class Scoring():

    def __init__(self):
        self.NUM_TYPES = 6
        self.q1_values = np.array([100, 75, 50, 28, 1])
        self.q2_values = np.array([50, 49, 48, 12, 9, 5, 0])
        self.cutoffs = np.array([[300, 223],
                                 [222, 134],
                                 [133, 87],
                                 [86, 45],
                                 [44, 17],
                                 [16, 1]])


    # Conditional scoring for question 3 based on score for q1 + q2
    def cond_scoring(self, num_samples, q_data):
        q3_values_option1 = np.array([0, 60, 30, 11, 0])
        q3_values_option2 = np.array([150, 71, 25, 11, 0])
        boundary = 37
       
        all_results = np.zeros(num_samples, dtype=int)
        all_scores = np.zeros(num_samples, dtype=int)
        individual_scores = np.zeros((num_samples, 3), dtype=int)

        for i in range(0, num_samples):
            # calculate the score
            individual_scores[i][0] = self.q1_values[q_data[i][0] - 1]
            individual_scores[i][1] = self.q2_values[q_data[i][1] - 1]
            score = individual_scores[i][0] + individual_scores[i][1] 

            # use different scoring based on answers to q1 and q2
            if score <= boundary:
                individual_scores[i][2] = q3_values_option1[q_data[i][2] - 1]
                score += individual_scores[i][2]
            else:
                individual_scores[i][2] = q3_values_option2[q_data[i][2] - 1]
                score += individual_scores[i][2]

            # determine skin type
            all_results[i] = self._which_skin_type(score)

            all_scores[i] = score
        
        return all_results, all_scores, individual_scores


    # Original scoring algorithm developed by Mark Sanchez
    def original_scoring(self, num_samples, q_data):
        q3_values = np.array([150, 71, 25, 11, 0])

        all_results = np.zeros(num_samples, dtype=int)
        all_scores = np.zeros(num_samples, dtype=int)
        individual_scores = np.zeros((num_samples, 3), dtype=int)

        for i in range(0, num_samples):
            # calculate the score
            score = (self.q1_values[q_data[i][0] - 1]
                    + self.q2_values[q_data[i][1] - 1] 
                    + q3_values[q_data[i][2] - 1])

            individual_scores[i][0] = self.q1_values[q_data[i][0] - 1]
            individual_scores[i][1] = self.q2_values[q_data[i][1] - 1]
            individual_scores[i][2] = q3_values[q_data[i][2] - 1]

            # determine skin type
            all_results[i] = self._which_skin_type(score)
            
            all_scores[i] = score
        
        return all_results, all_scores, individual_scores


    # Used by scoring algorithms to return a skin type based on their score
    def _which_skin_type(self, score):
        for i in range(self.NUM_TYPES):
            if score <= self.cutoffs[i, 0] and score >= self.cutoffs[i, 1]:
                skintype = i +1
                return skintype
        
        print("Score out of bounds!! Score = ", score)
        return 0
        

    # Returns to score for q1 + q2 ONLY. Used for analysis of how
    # participants respond to q3.
    def score_1_and_2(self, num_samples, q_data):
        score_1_and_2_only = np.zeros(num_samples, dtype=int)
        
        for i in range(0, num_samples):
            # calculate the score
            score_1_and_2_only[i] = (self.q1_values[q_data[i][0] - 1]
                    + self.q2_values[q_data[i][1] - 1]) 

        return score_1_and_2_only

    # Return type cutoffs
    def get_cutoffs(self):
        return self.cutoffs
